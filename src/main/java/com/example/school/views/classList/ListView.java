package com.example.school.views.classList;

import com.example.school.model.Class;
import com.example.school.service.ClassService;
import com.example.school.views.Layout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.spring.annotation.UIScope;
import de.codecamp.vaadin.serviceref.ServiceRef;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.validation.constraints.Null;
import java.util.Collection;
import java.util.List;

@Component
//@Scope("prototype")
@Route(value = "", layout = Layout.class)
@PageTitle("Class List | School System")
public class ListView extends VerticalLayout {

    //StudentForm form;
    Grid<Class> grid = new Grid<>(Class.class);
    TextField filterText = new TextField();

     @Autowired
     private ServiceRef<ClassService> classService;


    public ListView() {
        addClassName("classList-view");
        setSizeFull();
        configureGrid();


//        form = new StudentForm(companyService.findAll());
//        form.addListener(StudentForm.SaveEvent.class, this::saveContact);
//        form.addListener(StudentForm.DeleteEvent.class, this::deleteContact);
//        form.addListener(StudentForm.CloseEvent.class, e -> closeEditor());

        Div content = new Div(grid /*, form*/);
        content.addClassName("content");
        content.setSizeFull();

        add(getToolBar(), content);
        updateList();
        closeEditor();
    }

//    private void deleteContact(StudentForm.DeleteEvent evt) {
//        contactService.delete(evt.getContact());
//        updateList();
//        closeEditor();
//    }

//    private void saveContact(StudentForm.SaveEvent evt) {
//        contactService.save(evt.getContact());
//        updateList();
//        closeEditor();
//    }

    private HorizontalLayout getToolBar() {
        filterText.setPlaceholder("Filter by name...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addContactButton = new Button("Add contact", click -> addContact());

        HorizontalLayout toolbar = new HorizontalLayout(filterText, addContactButton);
        toolbar.addClassName("toolbar");
        return toolbar;
    }

    private void addContact() {
//        grid.asSingleSelect().clear();
//        editContact(new Contact());
    }

    private void configureGrid() {
        grid.addClassName("classs-grid");
        grid.setSizeFull();
       // grid.removeColumnByKey("studentList");
        grid.setColumns("classId","name");
//        grid.addColumn(classs -> {
//            Company company = contact.getCompany();
//            return company == null ? "-" : company.getName();
//        }).setHeader("Company");

        grid.getColumns().forEach(col -> col.setAutoWidth(true));

        //grid.asSingleSelect().addValueChangeListener(evt -> editContact(evt.getValue()));
    }

    private void editContact(Class classs) {
        if (classs == null) {
            closeEditor();
        } else {
           // form.setContact(contact);
        //    form.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor() {
       // form.setContact(null);
      //  form.setVisible(false);
        removeClassName("editing");
    }

    private void updateList()
    {

        //grid.setItems(classService.get().findAll(/*filterText.getValue()*/));
    }

}
