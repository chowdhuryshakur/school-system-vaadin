package com.example.school.views.studentList;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;

public class StudentForm extends FormLayout {

    TextField firstName = new TextField("First name");
    TextField lastName = new TextField("Last name");
    EmailField email = new EmailField("Email");
//    ComboBox<Contact.Status> status = new ComboBox<>("Status");
//    ComboBox<Company> company = new ComboBox<>("Company");

    Button save = new Button("Save");
    Button delete = new Button("Delete");
    Button close = new Button("Cancel");
//
//    Binder<Contact> binder = new BeanValidationBinder<>(Contact.class);
//    private Contact contact;

//    public StudentForm(List<Company> companies) {
//        addClassName("contact-form");
//
//        binder.bindInstanceFields(this);
//        status.setItems(Contact.Status.values());
//        company.setItems(companies);
//        company.setItemLabelGenerator(Company::getName);
//
//        add(
//            firstName,
//            lastName,
//            email,
//            status,
//            company,
//            createButtonsLayout()
//        );
//    }
//
//    public void setContact(Contact contact) {
//        this.contact = contact;
//        binder.readBean(contact);
//    }
//
//    private Component createButtonsLayout() {
//        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);
//        delete.addThemeVariants(ButtonVariant.LUMO_ERROR);
//        close.addThemeVariants(ButtonVariant.LUMO_TERTIARY);
//
//        save.addClickShortcut(Key.ENTER);
//        close.addClickShortcut(Key.ESCAPE);
//
//        save.addClickListener(click -> validateAndSave());
//        delete.addClickListener(click -> fireEvent(new DeleteEvent(this, contact)));
//        close.addClickListener(click -> fireEvent(new CloseEvent(this)));
//
//        binder.addStatusChangeListener(evt -> save.setEnabled(binder.isValid()));
//
//        return new HorizontalLayout(save, delete, close);
//    }
//
//    private void validateAndSave() {
//
//      try {
//        binder.writeBean(contact);
//        fireEvent(new SaveEvent(this, contact));
//      } catch (ValidationException e) {
//        e.printStackTrace();
//      }
//    }
//
//    // Events
//    public static abstract class ContactFormEvent extends ComponentEvent<StudentForm> {
//      private Contact contact;
//
//      protected ContactFormEvent(StudentForm source, Contact contact) {
//        super(source, false);
//        this.contact = contact;
//      }
//
//      public Contact getContact() {
//        return contact;
//      }
//    }
//
//    public static class SaveEvent extends ContactFormEvent {
//      SaveEvent(StudentForm source, Contact contact) {
//        super(source, contact);
//      }
//    }
//
//    public static class DeleteEvent extends ContactFormEvent {
//      DeleteEvent(StudentForm source, Contact contact) {
//        super(source, contact);
//      }
//
//    }
//
//    public static class CloseEvent extends ContactFormEvent {
//      CloseEvent(StudentForm source) {
//        super(source, null);
//      }
//    }
//
//    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
//                                                                  ComponentEventListener<T> listener) {
//      return getEventBus().addListener(eventType, listener);
//    }
}
