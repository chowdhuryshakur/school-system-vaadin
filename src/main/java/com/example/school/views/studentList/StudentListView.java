package com.example.school.views.studentList;

import com.example.school.model.Student;
import com.example.school.service.ClassService;
import com.example.school.service.StudentService;
import com.example.school.views.Layout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import de.codecamp.vaadin.serviceref.ServiceRef;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
@Route(value = "studentList", layout = Layout.class)
@PageTitle("Student List | School System")
public class StudentListView extends VerticalLayout {

    //StudentForm form;
    Grid<Student> grid = new Grid<>(Student.class);
    TextField filterText = new TextField();

    @Autowired
    private ServiceRef<StudentService> studentService;

    public StudentListView() {
        addClassName("classList-view");
        setSizeFull();
        configureGrid();


//        form = new StudentForm(companyService.findAll());
//        form.addListener(StudentForm.SaveEvent.class, this::saveContact);
//        form.addListener(StudentForm.DeleteEvent.class, this::deleteContact);
//        form.addListener(StudentForm.CloseEvent.class, e -> closeEditor());

        Div content = new Div(grid /*, form*/);
        content.addClassName("content");
        content.setSizeFull();

        add(getToolBar(), content);
        //updateList();
        closeEditor();
    }

//    private void deleteContact(StudentForm.DeleteEvent evt) {
//        contactService.delete(evt.getContact());
//        updateList();
//        closeEditor();
//    }

//    private void saveContact(StudentForm.SaveEvent evt) {
//        contactService.save(evt.getContact());
//        updateList();
//        closeEditor();
//    }

    private HorizontalLayout getToolBar() {
        filterText.setPlaceholder("Filter by name...");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        Button addContactButton = new Button("Add contact", click -> addContact());

        HorizontalLayout toolbar = new HorizontalLayout(filterText, addContactButton);
        toolbar.addClassName("toolbar");
        return toolbar;
    }

    private void addContact() {
//        grid.asSingleSelect().clear();
//        editContact(new Contact());
    }

    private void configureGrid() {
        grid.addClassName("classs-grid");
        grid.setSizeFull();
        grid.setColumns("studentId","firstName","rollNo","inClass");
//        grid.addColumn(contact -> {
//            Company company = contact.getCompany();
//            return company == null ? "-" : company.getName();
//        }).setHeader("Company");

        grid.getColumns().forEach(col -> col.setAutoWidth(true));

        //grid.asSingleSelect().addValueChangeListener(evt -> editContact(evt.getValue()));
    }

    private void editContact(Class classs) {
        if (classs == null) {
            closeEditor();
        } else {
           // form.setContact(contact);
        //    form.setVisible(true);
            addClassName("editing");
        }
    }

    private void closeEditor() {
       // form.setContact(null);
      //  form.setVisible(false);
        removeClassName("editing");
    }

    private void updateList() {
        grid.setItems(studentService.get().findAll());
    }

}
