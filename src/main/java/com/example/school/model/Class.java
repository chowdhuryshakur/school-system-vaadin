package com.example.school.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.persistence.Entity;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
//@EqualsAndHashCode(of = {"classId"})
@Entity
public class Class {
    @Id
    private String classId;
    private String name;
   // private List<Student> studentList;
}

